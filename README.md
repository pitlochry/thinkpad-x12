# thinkpad x12 detachable gen 1
tested on fedora 34-36, kernel 5.11-5.12,5.17-5.19, gnome 40-42, wayland, bios ..


### working out of the box
- touch screen
- keyboard with backlight, FnLock, suspend
- touch pad
- fingerprint sensor
- suspend and suspend on lid close
- pen
- wifi
- bluetooth
- front rgb/ir camera
- microphone
- auto-rotation
- trackpoint middle click
- trackpoint and trackpoint left/right click (since kernel 5.17 or with tweaks)
- internal speakers (since kernel 5.17 or with tweaks)


### does not yet work
- s3 suspend
- volume buttons
- rear camera
- Fn+F4/F8/F10/F11/F12

---


## tweaks (no more necessary since kernel 5.17)
clone this repository:
```
git clone https://gitlab.com/pitlochry/thinkpad-x12.git
```
and change into its directory via `cd thinkpad-x12`.


### trackpoint and trackpoint left/right click 
install required packages and build-dependencies (on fedora):
```
sudo dnf install git curl sed elfutils-devel openssl-devel perl-devel perl-generators pesign ncurses-devel dwarves zstd
sudo dnf groupinstall "Development Tools" "C Development Tools and Libraries" 
```
clone the mainline stable kernel repo:
```
git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
```
and change into its directory via `cd linux-stable`. If you already have the sources checked out from a previous build and want to re-compile the kernel for an updated version, you can run `git fetch` inside the repository to update it.

check out your desired version, replacing "x.y.z" with the desired version:
```
git checkout vx.y.z
```
optionally you can create a new branch, e.g. via `git switch -c vx.y.z-x12` #does this work with the patches a/b?

apply the kernel patch:
```
patch -p1 < ../trackpoint.patch
```
obtain the current kernel configuration of your distribution:
```
cp -v /boot/config-$(uname -r) .config
```
compile the kernel and headers:
```
make -j `getconf _NPROCESSORS_ONLN` bzImage
make -j `getconf _NPROCESSORS_ONLN` modules
```
install the kernel and headers:
```
sudo make -j `getconf _NPROCESSORS_ONLN` modules_install
sudo make -j `getconf _NPROCESSORS_ONLN` install
```
update grub (on fedora):
```
sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
```

### internal speakers
following command will make internal speakers work for some seconds, it keeps working while apps use sound device:
```
sudo python3 applyverbs.py alc285.txt 
```

which can be found e.g. [here](https://github.com/thesofproject/linux/files/6314986/verbs.zip). It will make internal speakers work for some seconds, it keeps working while apps use sound device. It might be solved in linux 5.13, see e.g. [here](https://github.com/thesofproject/linux/issues/2748).
